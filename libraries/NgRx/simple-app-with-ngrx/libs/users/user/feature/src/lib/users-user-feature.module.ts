import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersUserModelModule } from '@simple-app-with-ngrx/users/user/model';
import { UserComponent } from './containers/user/user.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatNativeDateModule } from '@angular/material/core';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatInputModule } from '@angular/material/input';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { UsersUserDataAccessModule } from '@simple-app-with-ngrx/users/user/data-access';
import { HelloComponent } from './containers/hello/hello.component';

@NgModule({
  declarations: [UserComponent, HelloComponent],
  imports: [
    CommonModule,
    UsersUserModelModule,
    MatFormFieldModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatButtonModule,
    MatIconModule,
    MatInputModule,
    FormsModule,
    ReactiveFormsModule,
    UsersUserDataAccessModule,
  ],
})
export class UsersUserFeatureModule {}
