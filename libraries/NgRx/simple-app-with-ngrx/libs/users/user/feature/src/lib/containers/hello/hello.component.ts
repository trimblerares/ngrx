import { Component, OnDestroy, OnInit } from '@angular/core';
import { UsersFacade } from '@simple-app-with-ngrx/users/user/data-access';
import { User } from '@simple-app-with-ngrx/users/user/model';
import { Subject, takeUntil } from 'rxjs';

@Component({
  selector: 'simple-app-with-ngrx-hello',
  templateUrl: './hello.component.html',
  styleUrls: ['./hello.component.scss'],
})
export class HelloComponent implements OnInit, OnDestroy {
  destroy$: Subject<void> = new Subject();
  user: User | undefined = undefined;

  constructor(private userFacade: UsersFacade) {}

  ngOnInit(): void {
    this.userFacade.getUser$
      .pipe(takeUntil(this.destroy$))
      .subscribe((user) => (this.user = user));
  }

  ngOnDestroy(): void {
    this.destroy$.next();
    this.destroy$.unsubscribe();
  }
}
