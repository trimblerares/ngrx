import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { UsersFacade } from '@simple-app-with-ngrx/users/user/data-access';
import { User } from '@simple-app-with-ngrx/users/user/model';
import { Observable } from 'rxjs';

@Component({
  selector: 'simple-app-with-ngrx-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.scss'],
})
export class UserComponent implements OnInit {
  user$: Observable<User | undefined> = this.userFacade.getUser$;
  user: User | undefined = undefined;

  userFormGroup: FormGroup = new FormGroup({});
  constructor(
    private formBuilder: FormBuilder,
    private userFacade: UsersFacade,
    private router: Router
  ) {}

  ngOnInit(): void {
    this.userFormGroup = this.formBuilder.group({
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      age: ['', [Validators.required, Validators.pattern('^[0-9]*$')]],
    });
  }

  addUser() {
    this.user = {
      firstName: this.userFormGroup.value.firstName,
      lastName: this.userFormGroup.value.lastName,
      age: this.userFormGroup.value.age,
    };
    this.userFacade.addUser(this.user);
    this.router.navigate(['hello']);
  }
}
