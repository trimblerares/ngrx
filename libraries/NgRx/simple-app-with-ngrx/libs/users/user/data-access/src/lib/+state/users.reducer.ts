import { EntityState, EntityAdapter, createEntityAdapter } from '@ngrx/entity';
import { createReducer, on, Action } from '@ngrx/store';

import * as UsersActions from './users.actions';
import { User } from '@simple-app-with-ngrx/users/user/model';

export const USERS_FEATURE_KEY = 'users';

export interface State {
  user: User | undefined;
}

export interface UsersPartialState {
  readonly [USERS_FEATURE_KEY]: State;
}

export const initialState: State = {
  user: undefined,
};

const usersReducer = createReducer(
  initialState,
  on(UsersActions.addUser, (state, { user }) => ({ user: user }))
);

export function reducer(state: State | undefined, action: Action) {
  return usersReducer(state, action);
}
