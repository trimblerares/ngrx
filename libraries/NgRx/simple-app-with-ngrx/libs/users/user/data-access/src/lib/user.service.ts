import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { User } from '@simple-app-with-ngrx/users/user/model';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  readonly baseUrl = 'https://localhost:44322';
  readonly httpOptions = {
    headers: new HttpHeaders({
      'Content-Type': 'application/json',
    }),
  };

  constructor(private httpClient: HttpClient) {}

  addUser(user: User) {
    return this.httpClient.post(this.baseUrl + '/User', user, this.httpOptions);
  }

  getUser(id: string): Observable<User> {
    return this.httpClient.get<User>(
      this.baseUrl + '/User/' + id,
      this.httpOptions
    );
  }
}
