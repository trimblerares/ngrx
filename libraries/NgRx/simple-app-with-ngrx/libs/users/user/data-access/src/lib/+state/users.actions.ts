import { createAction, props } from '@ngrx/store';
import { User } from '@simple-app-with-ngrx/users/user/model';

/*export const init = createAction('[Users Page] Init');

export const loadUsersSuccess = createAction(
  '[Users/API] Load Users Success',
  props<{ users: UsersEntity[] }>()
);

export const loadUsersFailure = createAction(
  '[Users/API] Load Users Failure',
  props<{ error: any }>()
);*/

export const addUser = createAction('[User] addUser', props<{ user: User }>());
export const getUser = createAction('[User] getUser');
