import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UsersUserModelModule } from '@simple-app-with-ngrx/users/user/model';
import { StoreModule } from '@ngrx/store';
import { EffectsModule } from '@ngrx/effects';
import * as fromUsers from './+state/users.reducer';
import { UsersEffects } from './+state/users.effects';
import { UsersFacade } from './+state/users.facade';
import { HttpClientModule } from '@angular/common/http';

@NgModule({
  imports: [
    CommonModule,
    UsersUserModelModule,
    StoreModule.forFeature(fromUsers.USERS_FEATURE_KEY, fromUsers.reducer),
    EffectsModule.forFeature([UsersEffects]),
    HttpClientModule,
  ],
  providers: [UsersFacade],
})
export class UsersUserDataAccessModule {}
