import { Component } from '@angular/core';

@Component({
  selector: 'simple-app-with-ngrx-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  title = 'simple-app-ngrx';
}
