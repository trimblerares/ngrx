import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {
  HelloComponent,
  UserComponent,
} from '@simple-app-with-ngrx/users/user/feature';

const routes: Routes = [
  { path: '', component: UserComponent, pathMatch: 'full' },
  { path: 'hello', component: HelloComponent },
  { path: '**', redirectTo: '' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
