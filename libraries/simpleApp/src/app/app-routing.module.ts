import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { UserAddedComponent } from './features/user-added/user-added.component';
import { UserFormComponent } from './features/user-form/user-form.component';

const routes: Routes = [
  { path: '', component: UserFormComponent, pathMatch: 'full' },
  { path: 'hello', component: UserAddedComponent },
  { path: '**', redirectTo: '' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
