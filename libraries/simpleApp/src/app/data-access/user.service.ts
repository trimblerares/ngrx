import { Injectable } from '@angular/core';
import { User } from '../models/user';

@Injectable({
  providedIn: 'root',
})
export class UserService {
  user: User = { id: '', firstName: '', lastName: '', age: '' };

  constructor() {}

  getUsers() {
    return this.user;
  }

  addUser(user: User) {
    this.user = user;
  }
}
