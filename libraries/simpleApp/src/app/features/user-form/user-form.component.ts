import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { UserService } from 'src/app/data-access/user.service';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-user-form',
  templateUrl: './user-form.component.html',
  styleUrls: ['./user-form.component.scss'],
})
export class UserFormComponent implements OnInit {
  user: User = { id: '', firstName: '', lastName: '', age: '' };

  userFormGroup: FormGroup = new FormGroup({});

  constructor(
    private formBuilder: FormBuilder,
    private userService: UserService
  ) {}

  ngOnInit(): void {
    this.userFormGroup = this.formBuilder.group({
      firstName: ['', [Validators.required]],
      lastName: ['', [Validators.required]],
      age: ['', [Validators.required, Validators.pattern('^[0-9]*$')]],
    });
  }

  addUser() {
    this.user.firstName = this.userFormGroup.value.firstName;
    this.user.lastName = this.userFormGroup.value.lastName;
    this.user.age = this.userFormGroup.value.age;

    this.userService.addUser(this.user);
  }
}
