import { Component, OnInit } from '@angular/core';
import { UserService } from 'src/app/data-access/user.service';
import { User } from 'src/app/models/user';

@Component({
  selector: 'app-user-added',
  templateUrl: './user-added.component.html',
  styleUrls: ['./user-added.component.scss'],
})
export class UserAddedComponent implements OnInit {
  user: User = { id: '', firstName: '', lastName: '', age: '' };

  constructor(private userService: UserService) {}

  ngOnInit(): void {
    this.user = this.userService.getUsers();
  }
}
