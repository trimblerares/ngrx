﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UsersAPI.DomainModels;
using UsersAPI.Repositories;

namespace UsersAPI.Processors
{
    public class UserProcessor : IUserProcessor
    {
        private readonly IUserRepository _userRepository;
        private readonly IMapper _mapper;

        public UserProcessor(IMapper mapper,IUserRepository repository)
        {
            _userRepository = repository;
            _mapper = mapper;
        }

        public async Task<UserView> AddUserAsync(UserView user)
        {
            var userView = _mapper.Map<UserDomain>(user);
            return _mapper.Map<UserView>(await _userRepository.AddUserAsync(userView));
        }

        public async Task<UserView> GetUserAsync(string id)
        {
            return _mapper.Map<UserView>(await _userRepository.GetUserAsync(id));
        }
    }
}
