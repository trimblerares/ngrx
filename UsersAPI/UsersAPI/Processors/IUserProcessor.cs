﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UsersAPI.DomainModels;

namespace UsersAPI.Processors
{
    public interface IUserProcessor
    {
        Task<UserView> GetUserAsync(string id);
        Task<UserView> AddUserAsync(UserView user);
    }
}
