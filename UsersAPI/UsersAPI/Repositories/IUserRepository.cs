﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UsersAPI.DomainModels;

namespace UsersAPI.Repositories
{
    public interface IUserRepository
    {
        Task<UserDomain> GetUserAsync(string id);
        Task<UserDomain> AddUserAsync(UserDomain user);
    }
}
