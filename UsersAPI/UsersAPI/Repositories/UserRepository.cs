﻿using MongoDB.Driver;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UserAPI.Settings;
using UsersAPI.DomainModels;

namespace UsersAPI.Repositories
{
    public class UserRepository : IUserRepository
    {
        private readonly IMongoCollection<UserDomain> _users;

        public UserRepository(IMongoDBSettings settings)
        {
            var client = new MongoClient(settings.ConnectionString);
            var database = client.GetDatabase(settings.DatabaseName);

            _users = database.GetCollection<UserDomain>(settings.UserCollectionName);
        }

        public async Task<UserDomain> AddUserAsync(UserDomain user)
        {
            user.Id = Guid.NewGuid().ToString();
            await _users.InsertOneAsync(user);
            return user;
        }

        public async Task<UserDomain> GetUserAsync(string id)
        {
            return (await _users.FindAsync(user => user.Id == id)).FirstOrDefault();
        }
    }
}
