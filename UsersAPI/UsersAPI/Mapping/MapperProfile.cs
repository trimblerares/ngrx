﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UsersAPI.DomainModels;

namespace UserAPI.Mapping
{
    public class MapperProfile:Profile
    {
        public MapperProfile()
        {
            CreateMap<UserView, UserDomain>();
            CreateMap<UserDomain, UserView>();
        }
    }
}
