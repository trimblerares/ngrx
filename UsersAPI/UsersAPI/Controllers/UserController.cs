﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using UsersAPI.DomainModels;
using UsersAPI.Processors;

namespace UsersAPI.Controllers
{
    [Route("[controller]")]
    [ApiController]
    public class UserController : ControllerBase
    {
        private readonly IUserProcessor _userProcessor;

        public UserController(IUserProcessor userProcessor)
        {
            _userProcessor = userProcessor;
        }

        /// <summary>
        /// gets user by id
        /// </summary>
        /// <param name="id">id to match</param>
        /// <returns>Code 200</returns>
        [HttpGet("{id}")]
        public async Task<IActionResult> GetUserAsync(string id)
        {
            return Ok(await _userProcessor.GetUserAsync(id));
        }

        /// <summary>
        /// adds user 
        /// </summary>
        /// <param name="user">user info</param>
        /// <returns>Code 400 if user is null and code 200</returns>
        [HttpPost]
        public async Task<IActionResult> AddUserAsync([FromBody] UserView user)
        {
            if (user == null)
            {
                return BadRequest();
            }
            var userCreated = await _userProcessor.AddUserAsync(user);

            return CreatedAtRoute(userCreated, userCreated);
        }
    }
}
